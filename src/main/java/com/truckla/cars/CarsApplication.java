package com.truckla.cars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"com.truckla"})
public class CarsApplication {

	public static void main(String[] args) {

		SpringApplication.run(CarsApplication.class, args);
	}

}
